
@extends('layouts.adminLayout')
@section('content')
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>General Form</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Ajouter question</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{ route('admin.questions.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" class="form-control" id="title" placeholder="Ajoute un titre">
                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-group">
                        <label for="rang">Rang</label>
                        <input type="number" name="rang" class="form-control" id="rang" min="1" value="1">
                        @if ($errors->has('rang'))
                            <span class="help-block">
                                <strong>{{ $errors->first('rang') }}</strong>
                            </span>
                        @endif
                      </div>
                     
                  
                    <div class="form-group">
                      <label for="country" class="col-md-4 control-label">Chapitre :</label>
                      <div class="col-md-6">
                          <select name="chapitre_id" id="chapitre_id" class="form-control">
                              @foreach($chapitres as $chapitre)
                                  <option value="{{ $chapitre->id }}">{{ $chapitre->title }}</option>
                              @endforeach
                          </select>
                      </div>
                </div>


                  
                  </div>
                 
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
           
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
