
@extends('layouts.adminLayout')
@section('content')
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>General Form</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Ajouter reponse</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{ route('admin.reponses.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="reponse">Reponse</label>
                        <input type="text" name="reponse" class="form-control" id="reponse" placeholder="Ajoute une reponse">
                        @if ($errors->has('reponse'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('reponse') }}</strong>
                                    </span>
                                @endif
                      </div>

                  <div class="form-group">
                        <label>
                          <input type="radio" name="valide" value="True" checked>
                          True
                        </label>
                        <label>
                            <input type="radio" name="valide"  value="False" >
                            False
                          </label>
                      </div>
                  
                  <div class="form-group">
                    <label for="country" class="col-md-4 control-label">Question :</label>
                    <div class="col-md-6">
                        <select name="question_id" id="question_id" class="form-control">
                            @foreach($questions as $question)
                                <option value="{{ $question->id }}">{{ $question->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                  
                  </div>
                 
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
           
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
