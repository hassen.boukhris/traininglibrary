@extends('layouts.adminLayout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>DataTables</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <a class="btn btn-block bg-gradient-success" href="{{ route('admin.reponses.create') }}">
                Ajouter
              </a>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Liste des reponses</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Reponse</th>
                  <th>Valide</th>
                  <th>Id Question</th>

                  <th>Action</th>
                </tr>
                </thead>

                <tbody>
                    
                    @foreach ($data as $item)
                        
                  
                    <tr>
                      <td>{{$item -> id}}</td>
                      <td>{{$item -> reponse}}</td>
                      <td>{{$item -> valide}}</td>
                      <td>{{$item -> question_id}}</td>

                      <td>
                        <a href="{{route('admin.reponses.show',['id'=>$item->id])}}" class = "btn btn-primary">Show</a>
                        <a href="{{route('admin.reponses.destroy',['id'=>$item->id])}}" class = "btn btn-danger">Delete</a>
                      </td>
                      </tr>


                    @endforeach

                    
                    </tbody>
                    <tfoot>
                    <tr>
                      
                    </tr>
                    </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

         
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection