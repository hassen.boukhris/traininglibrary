@extends('layouts.adminLayout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>DataTables</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <a class="btn btn-block bg-gradient-success" href="{{ route('admin.chapitres.create') }}">
                Ajouter
              </a>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Liste des chapitres</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>title</th>
                  <th>video_name</th>
                  <th>description</th>
                  <th>Rang</th>
                  <th>Action</th>
                </tr>
                </thead>

                <tbody>
                    
                    @foreach ($data as $item)
                        
                  
                    <tr>
                    <td>{{$item -> id}}</td>
                    <td>{{$item -> title}}</td>
                      <td>{{$item -> video_name}}</td>
                      <td>{{$item -> description}}</td>
                      <td>{{$item -> rang}}</td>
                      <td>
                        <a href="{{route('admin.chapitres.show',['id'=>$item->id])}}" class = "btn btn-primary">Show</a>
                        <a href="{{route('admin.chapitres.destroy',['id'=>$item->id])}}" class = "btn btn-danger">Delete</a>
                      </td>
                      </tr>

                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                      
                    </tr>
                    </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

         
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection