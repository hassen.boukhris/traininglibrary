<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
//Route::get('home', 'HomeController@index')->name('home');
// Route::group(['middleware' => ['auth']], function() {
//     Route::resource('roles','RoleController');
//     Route::resource('users','UserController');
//     //Route::resource('/users','UserController',['except' => ['show','create','store']]);
// });
/********* **********/
/*formation routes*/
/********* **********/
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth']], function () {
//Route::get('/', 'HomeController@index')->name('home');
Route::get('/dashboard', 'AdminController@dashboard')->name('dashboard');
Route::resource('roles','RoleController');
Route::resource('users','UserController');
Route::resource('categories','CategoryController');
Route::resource('formations','FormationController');
Route::resource('chapitres','ChapitreController');
Route::resource('questions','QuestionController');
Route::resource('reponses','ReponseController');


// Route::get('/chapitre', 'ChapitreController@index')->name('chapitres.index');
//Route::get('user/show', 'UserController@show')->name('show');

/********* **********/
/*category routes*/
/********* **********/

Route::group(['prefix' => 'category', 'as' => 'categories.'],  function () {
    Route::get('/create','CategoryController@create')->name('create');
    Route::post('/create','CategoryController@store')->name('store');
    Route::get('/{id}/show','CategoryController@show')->name('show');
    Route::get('/{id}/delete','CategoryController@destroy')->name('destroy');
    }); 
/********* **********/
/*formation routes*/
/********* **********/

Route::group(['prefix' => 'formation', 'as' => 'formations.'],  function () {
Route::get('/create','FormationController@create')->name('create');
Route::post('/create','FormationController@store')->name('store');
Route::get('/{id}/show','FormationController@show')->name('show');
Route::get('/{id}/delete','FormationController@destroy')->name('destroy');
}); 

/********* **********/
/*chapitre routes*/
/********* **********/
Route::group(['prefix' => 'chapitre', 'as' => 'chapitres.'],  function () {
Route::get('/create','ChapitreController@create')->name('chapitres.create');
Route::post('/create','ChapitreController@store')->name('chapitres.store');
Route::get('/{id}/show','ChapitreController@show')->name('show');
Route::get('/{id}/delete','ChapitreController@destroy')->name('destroy');
});
/********* **********/
/*question routes*/
/********* **********/
Route::group(['prefix' => 'question', 'as' => 'questions.'],  function () {
    Route::get('/create','QuestionController@create')->name('questions.create');
    Route::post('/create','QuestionController@store')->name('questions.store');
    Route::get('/{id}/show','QuestionController@show')->name('show');
    Route::get('/{id}/delete','QuestionController@destroy')->name('destroy');
    });

/********* **********/
/*reponse routes*/
/********* **********/
Route::group(['prefix' => 'reponse', 'as' => 'reponses.'],  function () {
    Route::get('/create','ReponseController@create')->name('reponses.create');
    Route::post('/create','ReponseController@store')->name('reponses.store');
    Route::get('/{id}/show','ReponseController@show')->name('show');
    Route::get('/{id}/delete','ReponseController@destroy')->name('destroy');
    });


// Route::get('/role', 'RoleController@index')->name('roles.index');
// Route::get('/create','RoleController@create')->name('roles.create');
// Route::post('/create','RoleController@store')->name('roles.store');
});
