<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapitre extends Model
{
    protected $fillable = ['title','description','video_name','video_type','pause_time','path','rang','formation_id'];
  
    public function formation()
    {
        return $this->belongsTo('App\Formation');
    }
    
    public function questions()
    {
        return $this->hasMany('App\Question','chapitre_id','id');
    }

}
