<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['title','rang','chapitre_id'];

    public function chapitre()
    {
        return $this->belongsTo('App\Chapitre');
    }
    public function reponses()
    {
        return $this->hasMany('App\Reponse','question_id','id');
    }
}
