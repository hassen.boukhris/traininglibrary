<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reponse extends Model
{
    protected $fillable = ['title','question_id'];

    public function question()
    {
        return $this->belongsTo('App\Question');
    }
    
}
