<?php

namespace App\Http\Controllers;

use App\Chapitre;
use App\Formation;
use Illuminate\Http\Request;

class ChapitreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Chapitre::all();
        return view('admin.chapitres.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $formations = Formation::all();
 
        // Envoi du formulaire
        return view('admin.chapitres.add', compact( 'formations'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $chapitre = new Chapitre();
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'video' => 'required',        ]);
        // $request->validate([
        //     'title' => 'required',
        //     'description' => 'required',
        //     'video' => 'required',
        // ]);   
    //    //dd($request->file('video')->getRealPath());
    //    // dd($file->getClientOriginalExtension());


        $file = $request->file('video');
        $title = $request->title;
        $description = $request->description;
        $video_name = $file->getClientOriginalName();
        $video_type = $file->getMimeType();
        $path = $request->file('video')->store('videos');
        $formation_id = $request->formation_id;
        $rang = $request->rang;
        $pause_time = 0;
        $video_name = time()."_".$video_name;
        $chapitre->video_name = $video_name;
        $chapitre->title = $title; 
        $chapitre->description = $description; 
        $chapitre->video_type = $video_type;
        $chapitre->path =$path;
        $chapitre->rang = $rang;
        $chapitre->pause_time = $pause_time;
        $chapitre->formation_id = $formation_id;


       
        
    //    // dd($path);


        $file->move(public_path('videos/chapitre/'),$video_name);
        $chapitre->save();
        return redirect()->route('admin.chapitres.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chapitre  $chapitre
     * @return \Illuminate\Http\Response
     */
    public function show(Chapitre $chapitre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chapitre  $chapitre
     * @return \Illuminate\Http\Response
     */
    public function edit(Chapitre $chapitre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chapitre  $chapitre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chapitre $chapitre)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chapitre  $chapitre
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $chapitre = Chapitre::find($id);
        //delete
        $chapitre->delete();
        return redirect()->route('admin.chapitres.index');
    }
}
