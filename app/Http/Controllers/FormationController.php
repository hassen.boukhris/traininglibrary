<?php

namespace App\Http\Controllers;

use App\Category;
use App\Formation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use SebastianBergmann\Environment\Console;

class FormationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        

        $data = Formation::all();
        return view('admin.formations.index',compact('data'));
        //dd($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
 
        // Envoi du formulaire
        return view('admin.formations.add', compact( 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formation = new Formation();
        $request->validate([
            'description' => 'required',
            'video' => 'required|image',
        ]);   
       //dd($request->file('video')->getRealPath());
       // dd($file->getClientOriginalExtension());


       $file= $request->file('video');
       $title = $request->title;

        $video_name = $file->getClientOriginalName();
        $description = $request->description;
        $video_type = $file->getMimeType();
        $size = $file->getSize(); 
        $path = $request->file('video')->store('videos');
        $category_id= $request->category_id;

        $formation->title =$title; 
        $formation->description =$description; 
        $formation->video_type =$video_type;
        $formation->size =$size;
        $formation->path =$path;
        $formation->category_id =$category_id;

       
        
       // dd($path);
        $video_name = time()."_".$video_name;
        $formation->video_name = $video_name;

        $file->move(public_path('videos'),$video_name);
        $formation->save();
        return redirect()->route('admin.formations.index');

        //dd($video_type);
        //$document->getRealPath();
        //$document->getClientOriginalName();
        //$document->getClientOriginalExtension();
        //dd($video_type);
        //print_r($request->file());
        //echo $request->file('video')->store('upload');
        //$formation->video_name = $$request->file()  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Formation  $formation
     * @return \Illuminate\Http\Response
     */
    public function show( $id)
    {
        $formation = Formation::find($id);
        return view('admin.formations.show',compact('formation'));
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Formation  $formation
     * @return \Illuminate\Http\Response
     */
    public function edit(Formation $formation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Formation  $formation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Formation $formation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Formation  $formation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Retrieve the formation
        $formation = Formation::find($id);
        //delete
        $formation->delete();
        return redirect()->route('admin.formations.index');
    }
}
