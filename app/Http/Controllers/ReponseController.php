<?php

namespace App\Http\Controllers;

use App\Question;
use App\Reponse;
use Illuminate\Http\Request;

class ReponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Reponse::all();
        return view('admin.reponses.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $questions = Question::all();
 
        // Envoi du formulaire
        return view('admin.reponses.add', compact( 'questions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reponse_obj = new Reponse();
        $reponse = $request->reponse;
        $is_valid = 0;
        $req_valide = $request->valide;
        if($req_valide)
            $is_valid = 1;
        
        $question_id = $request->question_id;
        $reponse_obj->reponse = $reponse; 
        $reponse_obj->valide = $is_valid;
        $reponse_obj->question_id = $question_id;

        $reponse_obj->save();

        return redirect()->route('admin.reponses.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reponse  $reponse
     * @return \Illuminate\Http\Response
     */
    public function show(Reponse $reponse)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reponse  $reponse
     * @return \Illuminate\Http\Response
     */
    public function edit(Reponse $reponse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reponse  $reponse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reponse $reponse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reponse  $reponse
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reponse $reponse)
    {
        //
    }
}
