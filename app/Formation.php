<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formation extends Model
{
    protected $fillable = ['video_name','description','video_type','pause_time','size','path','category_id'];
    
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
  
    public function chapitres()
    {
        return $this->hasMany('App\Chapitre','formation_id','id');
    }
}
